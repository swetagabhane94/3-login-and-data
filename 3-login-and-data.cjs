const fs = require("fs");
const path = require("path");

const fileName1 = path.resolve("./file1.cjs");
const fileName2 = path.resolve("./file2.cjs");

const lipsumFile = path.resolve("./lipsum.txt");
const newFile = path.resolve("./newFile.txt");

// Q1. Create 2 files simultaneously (without chaining).
// Wait for 2 seconds and starts deleting them one after another. (in order)
// (Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)

function problem1() {
  new Promise((resolve, reject) => {
    fs.writeFile(fileName1, "hello", "utf-8", (err) => {
      if (err) {
        reject(err);
        console.log(`Error occured in create file1 ${err}`);
      } else {
        console.log(`File1 created`);
        resolve();
      }
    });
  });

  new Promise((resolve, reject) => {
    fs.writeFile(fileName2, "hello! I am here", "utf-8", (err) => {
      if (err) {
        reject(err);
        console.log(`Error occured in create file2 ${err}`);
      } else {
        console.log(`File2 created`);
        resolve();
      }
    });
  });

  setTimeout(() => {
    fs.unlink(fileName1, (err) => {
      if (err) {
        console.log(`Error occured in delete file1 ${err}`);
      } else {
        console.log(`File1 deleted`);

        fs.unlink(fileName2, (err) => {
          if (err) {
            console.log(`Error occured in delete file2 ${err}`);
          } else {
            console.log(`File2 deleted`);
          }
        });
      }
    });
  }, 2 * 1000);
}

//problem1();

// Q2. Create a new file with lipsum data (you can google and get this).
// Do File Read and write data to another file
// Delete the original file
// Using promise chaining

function problem2() {
  let lipsumData =
    "Etiam tincidunt, sapien sed tristique placerat, lacus diam pretium tortor, et semper turpis nunc id ipsum. Suspendisse laoreet laoreet leo, sit amet mattis magna rhoncus quis. Vestibulum eleifend sed ex auctor semper. Vivamus imperdiet urna hendrerit mauris sollicitudin, nec ultrices ipsum scelerisque. Etiam non lacus mauris. In suscipit, lacus in aliquet vehicula, lacus risus sagittis purus, vel consequat eros arcu eget diam. Ut rutrum risus sed dictum cursus. In et leo et diam mattis rutrum nec id erat. Donec id dui justo";

  new Promise((resolve, reject) => {
    fs.writeFile(lipsumFile, lipsumData, "utf-8", (err) => {
      if (err) {
        reject(err);
        console.log(`Error occured in create lipsum file ${err}`);
      } else {
        console.log(`Lipsum file created`);
        resolve(lipsumData);
      }
    });
  })
    .then((lipsumData) => {
      return new Promise((resolve, reject) => {
        fs.readFile(lipsumFile, "utf-8", (err, lipsumData) => {
          if (err) {
            reject(err);
            console.log(`Error occured in read lipsum file ${err}`);
          } else {
            console.log(`Lipsum file read`);
            resolve(lipsumData);
          }
        });
      });
    })
    .then((data) => {
      return new Promise((resolve, reject) => {
        fs.writeFile(newFile, data, "utf-8", (err) => {
          if (err) {
            reject(err);
            console.log(`Error occured in create new file ${err}`);
          } else {
            console.log(`New file created`);
            resolve();
          }
        });
      });
    })
    .then(() => {
      return new Promise((resolve, reject) => {
        fs.unlink(lipsumFile, (err) => {
          if (err) {
            console.log(`Error occured in delete lipsum file ${err}`);
          } else {
            console.log(`Lipsum file deleted`);
          }
        });
      });
    });
}

//problem2();

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/
function login(user, val) {
    if (val % 2 === 0) {
      return Promise.resolve(user);
    } else {
      return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
      {
        id: 1,
        name: "Test",
      },
      {
        id: 2,
        name: "Test 2",
      },
    ]);
}

function problem3() {
    let user = "Test 3"
    login(user,3)

}
